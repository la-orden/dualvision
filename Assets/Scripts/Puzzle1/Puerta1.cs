using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta1 : MonoBehaviour
{
    public bool abre;

    public GameObject puerta1;

    void Start()
    {
        abre = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (abre == false)
        {
            puerta1.transform.localPosition = new Vector3(0, -90, 0);
            //  print("Se abrio la puerta master");
        }
    }
}
