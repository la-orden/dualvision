using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle1 : MonoBehaviour
{
    public bool Esto;
    public Walk walk;
    public VistaPizarron CanvaPiza;
    public Puerta1 puerta;
    public bool je;
    public BotonPizzaron but1;
    public int Cont;
    public bool ready;
    public bool next;
    public Text T1;
    public Text T2;
    public int apper = 1;
    public GameObject MapaLista;
    public Image map2;
    public Image map3;
    public Image map4;
    public Image map5;
    public Image map6;

    public GameObject particulas;

    public GameObject mision2;
    public GameObject mision3;
    public GameObject mision4;

    void Start()
    {
        je = true;
        Cont = 1;
        ready = false;
        next = false;
        T1.enabled = true;
        T2.enabled = false;
        MapaLista.SetActive(false);
        map2.enabled = false;
        map3.enabled = false;
        map4.enabled = false;
        map5.enabled = false;
        map6.enabled = false;

        mision2.SetActive(false);
        mision3.SetActive(false);
        mision4.SetActive(false);
    }

    void Update()
    {
        if (Esto == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                but1.wana = 1;
                je = true;
                print("funcona1 ");
                walk.enabled = false;
                CanvaPiza.Cartita2 = false;
                
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                
            }

            if(je == false)
            {
                print("funcona2 ");
                walk.enabled = true;
                CanvaPiza.Cartita2 = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
                apper = 2;
            }

            if(next == true)
            {
                print("funcona Next ");
                walk.enabled = true;
                CanvaPiza.Cartita2 = true;
                puerta.abre = false;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
                mision2.SetActive(true);
                Destroy(particulas.gameObject);

            }

            
        }

        if(Cont == 2)
        {
            map2.enabled = true;
        } 
        else if (Cont == 3)
        {
            map3.enabled = true;
        }
        else if (Cont == 4)
        {
            map4.enabled = true;
        }
        else if (Cont == 5)
        {
            map5.enabled = true;
        }
        else if (Cont == 6)
        {
            map6.enabled = true;
            ready = true;
        }


        if (ready == true)
        {
            but1.ready1 = true;

            T1.enabled = false;
            T2.enabled = true;
        }

        if (apper == 2)
        {
            MapaLista.SetActive(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Esto = true;

        }


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {

            Esto = false;

        }
    }
}
