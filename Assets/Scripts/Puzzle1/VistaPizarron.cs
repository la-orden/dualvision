using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VistaPizarron : MonoBehaviour
{
    private bool cartaEnabled;

    public GameObject pizarra;

    public bool Cartita2;

    void Start()
    {
        Cartita2 = true;

    }

    void Update()
    {
        if (Cartita2 == false)
        {
            cartaEnabled = true;
        }
        else
        {
            cartaEnabled = false;
        }

        if (cartaEnabled == true)
        {
            pizarra.SetActive(true);
        }
        else
        {
            pizarra.SetActive(false);
        }

    }
}
