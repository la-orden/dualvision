using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuColec : MonoBehaviour
{
    public GameObject Menu;
    public GameObject MenuCole;
    public Text Col1;
    public Text Col2;
    public Text Col3;
    public Text Col4;
    public Text Col5;
    public Image panel;

    public int pol1 = 1;
    public int pol2 = 1;
    public int pol3 = 1;
    public int pol4 = 1;
    public int pol5 = 1;

    public Button C1;
    public Button C2;
    public Button C3;
    public Button C4;
    public Button C5;

    public Image D1;
    public Image D2;
    public Image D3;
    public Image D4;
    public Image D5;

    void Start()
    {
        panel.enabled = false;
        Col1.enabled = false;
        Col2.enabled = false;
        Col3.enabled = false;
        Col4.enabled = false;
        Col5.enabled = false;

        C1.enabled = false;
        C2.enabled = false;
        C3.enabled = false;
        C4.enabled = false;
        C5.enabled = false;

        D1.enabled = false;
        D2.enabled = false;
        D3.enabled = false;
        D4.enabled = false;
        D5.enabled = false;

    }

    void Update()
    {
        if(pol1 == -1)
        {
            C1.enabled = true;
            D1.enabled = true;
        }
        if (pol2 == -1)
        {
            C2.enabled = true;
            D2.enabled = true;
        }
        if (pol3 == -1)
        {
            C3.enabled = true;
            D3.enabled = true;
        }
        if (pol4 == -1)
        {
            C4.enabled = true;
            D4.enabled = true;
        }
        if (pol5 == -1)
        {
            C5.enabled = true;
            D5.enabled = true;
        }
    }

    public void TaskOnClickBack()
    {
        Col1.enabled = false;
        Col2.enabled = false;
        Col3.enabled = false;
        Col4.enabled = false;
        Col5.enabled = false;
        Menu.SetActive(true);
        MenuCole.SetActive(false);
    }

    public void TaskOnClickCol1()
    {
        panel.enabled = true;
        Col1.enabled = true;
        Col2.enabled = false;
        Col3.enabled = false;
        Col4.enabled = false;
        Col5.enabled = false;
    }
    public void TaskOnClickCol2()
    {
        panel.enabled = true;
        Col1.enabled = false;
        Col2.enabled = true;
        Col3.enabled = false;
        Col4.enabled = false;
        Col5.enabled = false;
    }
    public void TaskOnClickCol3()
    {
        panel.enabled = true;
        Col1.enabled = false;
        Col2.enabled = false;
        Col3.enabled = true;
        Col4.enabled = false;
        Col5.enabled = false;
    }
    public void TaskOnClickCol4()
    {
        panel.enabled = true;
        Col1.enabled = false;
        Col2.enabled = false;
        Col3.enabled = false;
        Col4.enabled = true;
        Col5.enabled = false;
    }
    public void TaskOnClickCol5()
    {
        panel.enabled = true;
        Col1.enabled = false;
        Col2.enabled = false;
        Col3.enabled = false;
        Col4.enabled = false;
        Col5.enabled = true;
    }
}
