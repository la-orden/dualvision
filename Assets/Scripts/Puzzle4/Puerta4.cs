using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta4 : MonoBehaviour
{
    public bool abre;

    public GameObject puerta4;

    void Start()
    {
        abre = true;
    }

    void Update()
    {
        if (abre == false)
        {
            puerta4.transform.localPosition = new Vector3(0, -90, 0);
            //  print("Se abrio la puerta master");
        }
    }
}
