using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{
    public Walk walk;
    public GameObject Menu;
    public GameObject MenuColec;
    public Button hud;

    public bool onichan;



    void Start()
    {
        onichan = true;
        Menu.SetActive(false);
        MenuColec.SetActive(false);
        hud.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        if (onichan == true)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                hud.enabled = false;
                Menu.SetActive(true);
                walk.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }


    }

    public void TaskOnClickInicio()
    {
        SceneManager.LoadScene(0);
    }

    public void TaskOnClick1()
    {
        walk.enabled = true;
        Menu.SetActive(false);
        hud.enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
    }

    public void TaskOnClickColec()
    {
        Menu.SetActive(false);
        MenuColec.SetActive(true);
    }
    public void TaskOnClick2()
    {
        
    }
}
