using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VistaPuzzle2 : MonoBehaviour
{
    private bool cartaEnabled;

    public GameObject canva;

    public bool Cartita2;

    void Start()
    {
        Cartita2 = true;

    }

    void Update()
    {
        if (Cartita2 == false)
        {
            cartaEnabled = true;
        }
        else
        {
            cartaEnabled = false;
        }

        if (cartaEnabled == true)
        {
            canva.SetActive(true);
        }
        else
        {
            canva.SetActive(false);
        }
    }
}
