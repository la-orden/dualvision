using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle2 : MonoBehaviour
{
    public bool Esto;
    public Walk walk;
    public VistaPuzzle2 Canva2;
    public bool je;
    public BotonTacos but1;
    public int Cont;
    public GameObject imagen;
    public bool ready;
    public bool next;
    public Text T1;
    public Text T2;
    public int apper = 1;
    public GameObject TacosLista;
    public Image taco1;
    public Image taco2;
    public Image taco3;
    public Image taco4;

    void Start()
    {
        je = true;
        Cont = 0;
        ready = false;
        next = false;
        T1.enabled = true;
        T2.enabled = false;
        TacosLista.SetActive(false);
        taco1.enabled = false;
        taco2.enabled = false;
        taco3.enabled = false;
        taco4.enabled = false;
    }

    
    void Update()
    {
        if (Esto == true)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                but1.wana = 1;
                je = true;
                print("funcona1t ");
                walk.enabled = false;
                Canva2.Cartita2 = false;

                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

            }

            if (je == false)
            {
                print("funcona2t ");
                walk.enabled = true;
                Canva2.Cartita2 = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
                apper = 2;
            }

            if (next == true)
            {
                print("funcona Next t ");
                walk.enabled = true;
                Canva2.Cartita2 = true;

                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
           
            }


        }

        if (Cont == 1)
        {
            taco1.enabled = true;
        }
        else if (Cont == 2)
        {
            taco2.enabled = true;
        }
        else if (Cont == 3)
        {
            taco3.enabled = true;
        }
        else if (Cont == 4)
        {
            taco4.enabled = true;
            ready = true;
        }


        if (ready == true)
        {
            but1.ready1 = true;
            T1.enabled = false;
            T2.enabled = true;
        }

        if(apper == 2)
        {
            TacosLista.SetActive(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Esto = true;

        }


    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {

            Esto = false;

        }
    }
}
