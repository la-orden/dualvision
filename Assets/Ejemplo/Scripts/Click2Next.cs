﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Click2Next : MonoBehaviour
{

    public float transitionTime = 1f;
    public Animator transition;
    public string Escenita;

    void Update()
    {
           if(Input.GetMouseButtonDown(0))
         {
              LoadNextLevel();
         }
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(Escenita);
    }

}