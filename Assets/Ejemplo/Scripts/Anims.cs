﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anims : MonoBehaviour
{
    public GameObject Anim;

    void Update()
    {
        //ANIMACIONES

        if (Input.GetKey(KeyCode.W))
        {
            Anim.GetComponent<Animation>().CrossFade("playerWalk");

        }

        else
        {
            Anim.GetComponent<Animation>().CrossFade("playerIdle");

        }

        if (Input.GetKey(KeyCode.S))
        {
            Anim.GetComponent<Animation>().CrossFade("playerWalk");
        }

        if (Input.GetKey(KeyCode.D))
        {
            Anim.GetComponent<Animation>().CrossFade("playerWalk");
        }

        if (Input.GetKey(KeyCode.A))
        {
            Anim.GetComponent<Animation>().CrossFade("playerWalk");
        }

    }
}

