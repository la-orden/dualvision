﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public NPC npc;

    bool isTalking = false;

    float distance;
    float curResponseTracker = 0;

    public GameObject player;
    public GameObject dialogueUI;

    public Text npcName;
    public Text npcDialogueBox;
    public Text playerResponse;

    public Button Button1;
    public Button Button2;

    // Start is called before the first frame update
    void Start()
    {
        dialogueUI.SetActive(false);

        Button btn = Button1.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick1);

        Button btn2 = Button2.GetComponent<Button>();
        btn2.onClick.AddListener(TaskOnClick2);
    }

    void OnTriggerStay()
    {
        Debug.Log("Hellous");

        distance = Vector3.Distance(player.transform.position, this.transform.position);
        if(distance <= 20.5f)
        {
       /*     if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                Debug.Log("-1");
                curResponseTracker++;
                if (curResponseTracker >= npc.playerDialogue.Length - 1)
                {
                    curResponseTracker = npc.playerDialogue.Length - 1;
                }
            }
            else if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                curResponseTracker--;
                if(curResponseTracker < 0)
                {
                    curResponseTracker = 0;
                }
            } */
            //trigger dialogue
            if(Input.GetKeyDown(KeyCode.E) && isTalking==false)
            {
                StartConversation();
            }
            else if(Input.GetKeyDown(KeyCode.E) && isTalking == true)
            {
                EndDialogue();
            }

            if(curResponseTracker == 0 && npc.playerDialogue.Length >= 0)
            {
                playerResponse.text = npc.playerDialogue[0];
                if(Input.GetKeyDown(KeyCode.Space))
                {
                    npcDialogueBox.text = npc.dialogue[1];
                }
            }
            else if(curResponseTracker == 1 && npc.playerDialogue.Length >= 1)
            {
                playerResponse.text = npc.playerDialogue[1];
                if(Input.GetKeyDown(KeyCode.Space))
                {
                    npcDialogueBox.text = npc.dialogue[2];
                }
            }
            else if (curResponseTracker == 2 && npc.playerDialogue.Length >= 2)
            {
                playerResponse.text = npc.playerDialogue[2];
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    npcDialogueBox.text = npc.dialogue[3];
                }
            }

        }
    }

    void StartConversation()
    {
        isTalking = true;
        curResponseTracker = 0;
        dialogueUI.SetActive(true);
        npcName.text = npc.name;
        npcDialogueBox.text = npc.dialogue[0];
    }

    void EndDialogue()
    {
        isTalking = false;
        dialogueUI.SetActive(false);
    }

    void TaskOnClick1()
    {
        Debug.Log("anterior");

        curResponseTracker--;
        if (curResponseTracker < 0)
        {
            curResponseTracker = 0;
        }
    }

    void TaskOnClick2()
    {
        Debug.Log("Siguiente");

        Debug.Log("-1");
        curResponseTracker++;
        if (curResponseTracker >= npc.playerDialogue.Length - 1)
        {
            curResponseTracker = npc.playerDialogue.Length - 1;
        }

    }
}
