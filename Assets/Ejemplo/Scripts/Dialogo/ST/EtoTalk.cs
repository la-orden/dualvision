﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EtoTalk : MonoBehaviour
{
    public GameObject TalksScr;
    public GameObject STNotice;

    void Start()
    {
        TalksScr.SetActive(false);
        TalksScr.GetComponent<JustaComment>().enabled = false;
    }

    void Update()
    {
        
    }

    void OnTriggerEnter()
    {
        STNotice.SetActive(true);
    }

    void OnTriggerStay()
    {
        

        if (Input.GetKeyDown(KeyCode.E))
        {
            STNotice.SetActive(false);
            TalksScr.SetActive(true);
            TalksScr.GetComponent<JustaComment>().enabled = true;
        }
        else
        {
            //STNotice.SetActive(true);
        }
    }

    void OnTriggerExit()
    {
        STNotice.SetActive(false);
        TalksScr.GetComponent<JustaComment>().enabled = false;
        TalksScr.SetActive(false);
    }
}
