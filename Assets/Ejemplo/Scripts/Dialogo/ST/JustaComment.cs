﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JustaComment : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    //public TextMeshProUGUI NPCName;
    public string[] sentences;
    private int index;
    public float typingspeed;

    public GameObject continueButton;
    public GameObject endButton;

    public GameObject DialogueMenu;

    //public GameObject NPCName;

    public Camera camaraprincipal;
    public bool useStaticBillboard;

    void Start()
    {
        StartCoroutine(Type());
        camaraprincipal = Camera.main;
    }

    void OnEnable()
    {
        DialogueMenu.SetActive(true);
    }

    void Update()
    {

        if (textDisplay.text == sentences[index])
        {
            continueButton.SetActive(true);
        }

        if (!useStaticBillboard)
        {
            transform.LookAt(camaraprincipal.transform);
        }
        else
        {
            transform.rotation = camaraprincipal.transform.rotation;
        }

        transform.rotation = Quaternion.Euler(90f, transform.rotation.eulerAngles.y, 90f);
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingspeed);
        }
    }

    public void NextSentence()
    {
        continueButton.SetActive(false);

        //NPCName.SetActive(true);

        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            //textDisplay.text = "";
            continueButton.SetActive(false);
        }
    }

    public void EndConv()
    {
        DialogueMenu.SetActive(false);
    }

    void LateUpdate()
    {
       
    }
}
