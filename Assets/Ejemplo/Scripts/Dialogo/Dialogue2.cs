﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogue2 : MonoBehaviour
{
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typingspeed;

    public GameObject continueButton;
    public GameObject endButton;

    public GameObject AnswerButton1;
    public GameObject AnswerButton2;

    public GameObject DialogueMenu;

    public GameObject DialogueManager;
    public GameObject Choice1Manager;
    public GameObject Choice2Manager;

    public GameObject PlayerName;
    public GameObject NPCName;

    public GameObject Consecuencia1;
    public GameObject Consecuencia2;

    void Start()
    {
        StartCoroutine(Type());
        //sentences.Length = 1;
    }

    void Update()
    {
        if(textDisplay.text == sentences[index])
        {
            continueButton.SetActive(true);
        }
    }

    IEnumerator Type()
    {
        foreach(char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingspeed);
        }
    }

    public void NextSentence()
    {
        continueButton.SetActive(false);

        PlayerName.SetActive(false);
        NPCName.SetActive(true);

        if (index < sentences.Length - 1)
        {
            Debug.Log("Amarillo");
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            Debug.Log("Verde");

            PlayerName.SetActive(true);
            NPCName.SetActive(false);

            textDisplay.text = "";
            continueButton.SetActive(false);

            AnswerButton1.SetActive(true);
            AnswerButton2.SetActive(true);

            
        }
    }

    public void EndConv()
    {

        DialogueMenu.SetActive(false);
    }

    public void AnswerButtonOne()
    {
         

            AnswerButton1.SetActive(false);
            AnswerButton2.SetActive(false);
            Choice1Manager.SetActive(true);
            PlayerName.SetActive(false);
            NPCName.SetActive(true);
            DialogueManager.SetActive(false);

        ConsecuenciaV1();

    }

    public void ConsecuenciaV1()
    {
        Consecuencia1.SetActive(true);
        Debug.Log("Dialogo1Listo");
    }

    public void AnswerButtonTwo()
    {
        AnswerButton1.SetActive(false);
        AnswerButton2.SetActive(false);
        Choice2Manager.SetActive(true);
        PlayerName.SetActive(false);
        NPCName.SetActive(true);
        DialogueManager.SetActive(false);

        ConsecuenciaV2();
    }

    public void ConsecuenciaV2()
    {
        Consecuencia2.SetActive(true);
    }
}
