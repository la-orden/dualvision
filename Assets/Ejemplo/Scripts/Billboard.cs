﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    private Camera camaraprincipal;

    public bool useStaticBillboard;


    void Start()
    {
        camaraprincipal = Camera.main;
    }


    void LateUpdate()
    {
        if (!useStaticBillboard)
        {
            transform.LookAt(camaraprincipal.transform);
        } else
        {
            transform.rotation = camaraprincipal.transform.rotation;
        }

        transform.rotation = Quaternion.Euler(90f, transform.rotation.eulerAngles.y, 90f);
    }
}
