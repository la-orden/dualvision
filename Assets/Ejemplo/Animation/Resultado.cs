﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resultado : MonoBehaviour
{
    public string animahora;
    public Animator transition;
    //"Consecuencia1true"

    public GameObject cutScene;
    public GameObject Player;
    public GameObject NPC;
    public GameObject Portal;
    public GameObject Portal2;

    public float AnimTime = 1f;

    void Start()
    {
        
      Debug.Log("Animando");
        Player.SetActive(false);
        Portal.SetActive(false);
        Portal2.SetActive(true);
        NPC.SetActive(false);
        cutScene.SetActive(true);
        transition.SetTrigger(animahora);

        LoadNextLevel();
    }

    public void LoadNextLevel()
    {
        StartCoroutine(ResultadoFinal());
    }

    IEnumerator ResultadoFinal()
    {
        
        yield return new WaitForSeconds(AnimTime);
        Player.SetActive(true);
        cutScene.SetActive(false);
        Debug.Log("Animando");
    }


}
