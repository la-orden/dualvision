﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Animaciones : MonoBehaviour
{
    public GameObject Player;
    public GameObject cutSceneCam1;

    public float AnimTime = 1f;
    //  public GameObject cutSceneCam2; 

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            cutSceneCam1.SetActive(true);
           // Player.SetActive(false);
          //  StartCoroutine(FinishedCut());

        }
        else
        {
            cutSceneCam1.SetActive(false);
        }
      
    }

    IEnumerator FinishedCut()
    {
        yield return new WaitForSeconds(AnimTime);
       // Player.SetActive(true);
        cutSceneCam1.SetActive(false);
    }
/*
    IEnumerator FinishedCut2()
    {
        yield return new WaitForSeconds(12);
        Player.SetActive(true);
        cutSceneCam2.SetActive(false);
        SceneManager.LoadScene(2);
    }
    */

}
